const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAllUsers() {
        const items = UserRepository.getAll();
        if (!items || items.length === 0) {
            throw new Error('Users aren`t found!');
        }
        return items;
    }

    getUser(id) {
        const item = this.search({ id })
        if (!item) {
            throw new Error('User isn`t found!');
        }
        return item;
    }

    createUser(user) {
        if (this.search({ email: user.email })) {
            throw new Error('User is already exists with this email!');
        }
        if (this.search({ phoneNumber: user.phoneNumber })) {
            throw new Error('User is already exists with this phone number!');
        }
        const item = UserRepository.create(user);
        if (!item.id) {
          throw new Error('User isn`t created!');
        }
        return item;
    }

    deleteUser(id) {
        const item = this.search({ id })
        if (!item) {
            throw new Error('User isn`t found for deletion!');
        }
        const deletedItem = UserRepository.delete(id);
        if (!deletedItem) {
            throw new Error('User isn`t deleted!');
        }
        return deletedItem;
    }

    updateUser(id, userData) {
        const item = this.search({ id })
        if (!item) {
            throw new Error('User isn`t found!');
        }
        const updateItem = UserRepository.update(id, userData);
        if (!updateItem) {
            throw new Error('User isn`t updated!');
        }
        return updateItem;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();