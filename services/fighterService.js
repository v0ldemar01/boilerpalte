const { FighterRepository } = require('../repositories/fighterRepository');
const { fighterSource } = require('../constants/source');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAllFighters() {
        const items = FighterRepository.getAll();
        if(!items || items.length === 0) {
            throw new Error('Fighters aren`t found!');
        }
        return items;
    }

    getFighter(id) {
        const item = this.search({ id });
        if (!item) {
            throw new Error('Fighter isn`t found!');
        }
        return item;
    }

    createFighter(fighter) {
        if (this.search({ name: fighter.name })) {
            throw new Error('Fighter is already exists with this name!');
        };
        const { source } = this.generateSource();

        const item = FighterRepository.create({ ...fighter, source });
        if (!item.id) {
          throw new Error('Fighter isn`t created!');
        }
        return item;        
    }

    generateSource() {
        const random_index = Math.floor(Math.random() * fighterSource.length);
        return fighterSource[random_index];
    }

    deleteFighter(id) {
        const item = this.search({ id })
        if (!item) {
            throw new Error('Fighter isn`t found for deletion!');
        }       
        const deletedItem = FighterRepository.delete(id);
        if (!deletedItem) {
            throw new Error('Fighter isn`t deleted!');
        }
        return deletedItem;
    }

    updateFighter(id, fighterData) {
        const item = this.search({ id });
        if (!item) {
            throw new Error('Fighter isn`t found!');
        }        
        const updateItem = FighterRepository.update(id, fighterData);
        if (!updateItem) {
            throw new Error('Fighter isn`t updated!');
        }
        return updateItem;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();