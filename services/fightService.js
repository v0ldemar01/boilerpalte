const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
    getAllFights() {
        const items = FightRepository.getAll();
        if(!items || items.length === 0) {
            throw new Error('Fights aren`t found!');
        }
        return items;
    }

    getFight(id) {
        const item = this.search({ id });
        if (!item) {
            throw new Error('Fight isn`t found!');
        }
        return item;
    }

    createFight(fight) {
        const item = FightRepository.create(fight);
        if (!item.id) {
            throw new Error('Fight isn`t created!');
          }
        return item;
    }   

    deleteFight(id) {
        const item = this.search({ id });
        if (!item) {
            throw new Error('Fight isn`t found for deletion!');
        } 
        const deletedItem = FightRepository.delete(id);
        if (!deletedItem) {
            throw new Error('Fight isn`t deleted!');
        }
        return deletedItem;
    }

    doAttack(id, fightData) {
        const item = this.search({ id });
        if (!item) {
            throw new Error('Fight isn`t found!');
        }
        this.checkWinner(item, fightData);
        item.log = [...item.log, fightData];
        const updateItem = FightRepository.update(id, item);
        if (!updateItem){
            throw Error('Fight isn`t updated!');
        }
        return updateItem;
    }

    checkWinner(item, {fighter1Health, fighter2Health}) {
        if (fighter1Health === 0) {
            item.winner = 'fighter2';
        } else if (fighter2Health === 0) {
            item.winner = 'fighter1';
        }
    }
    
    search(search) {
        const item = FightRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FightersService();