const { checkSameObjKeys } = require('./checkObjects');

exports.createValid = (req, model, checkValid) => {
  const data = req.body;  
  const { id } = data;
  if (id) delete data.id;
  validateKeys(model, data);         
  checkValid(data);    
}

const validateKeys = (model, data) => {
  const {
      id,      
      ...newModel
  } = model;
  if (!checkSameObjKeys(newModel, data)) {
      throw new Error('There are no all or too many fields in data!');
  }
}