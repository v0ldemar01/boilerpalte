exports.checkSameObjKeys = (obj1, obj2) => {
  const obj1_keys = Object.keys(obj1);
  const obj2_keys = Object.keys(obj2);
  if (obj1_keys.length !== obj2_keys.length) {
    return false;
  } 
  return obj1_keys.every(key => obj2.hasOwnProperty(key));
}
