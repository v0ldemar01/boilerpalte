const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { 
  HTTP_STATUS_ERROR_BAD_REQUEST,
  HTTP_STATUS_ERROR_NOT_FOUND
} = require('../constants');

const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router.get(
  '/',
  (req, res, next) => {
    try {
      const fights = FightService.getAllFights();
      res.data = fights;
    } catch (err) {      
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/', 
  createFightValid,
  (req, res, next) => {
    try {
      const data = req.body;
      const newFight = FightService.createFight(data);
      res.data = newFight;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_BAD_REQUEST;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;
      const fight = FightService.getFight(id);
      res.data = fight;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id/attack',
  updateFightValid,
  (req, res, next) => {
    try {
      const { id } = req.params;
      const data = req.body;     
      const fight = FightService.doAttack(id, data);
      res.data = fight;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_BAD_REQUEST;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;
      const fight = FightService.deleteFight(id);
      res.data = fight;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;