const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { 
  HTTP_STATUS_ERROR_BAD_REQUEST,
  HTTP_STATUS_ERROR_NOT_FOUND
} = require('../constants');

const router = Router();

// TODO: Implement route controllers for user
router.get(
  '/',
  (req, res, next) => {
    try {
      const users = UserService.getAllUsers();
      res.data = users;
    } catch (err) {      
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/', 
  createUserValid,
  (req, res, next) => {
    try {
      const data = req.body;
      const newUser = UserService.createUser(data);
      res.data = newUser;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_BAD_REQUEST;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;
      const user = UserService.getUser(id);
      res.data = user;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateUserValid,
  (req, res, next) => {
    try {
      const { id } = req.params;
      const data = req.body;
      const user = UserService.updateUser(id, data);
      res.data = user;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;
      const user = UserService.deleteUser(id);
      res.data = user;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;