const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { 
    HTTP_STATUS_ERROR_UNAUTHORIZED
  } = require('../constants');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const data = req.body;
        const user = AuthService.login(data);
        res.data = user;
    } catch (err) {
        res.err = err;
        err.httpStatus = HTTP_STATUS_ERROR_UNAUTHORIZED;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;