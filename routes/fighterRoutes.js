const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { 
  HTTP_STATUS_ERROR_BAD_REQUEST,
  HTTP_STATUS_ERROR_NOT_FOUND
} = require('../constants');

const router = Router();

// TODO: Implement route controllers for fighter
router.get(
  '/',
  (req, res, next) => {
    try {
      const fighters = FighterService.getAllFighters();
      res.data = fighters;
    } catch (err) {      
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/', 
  createFighterValid,
  (req, res, next) => {
    try {
      const data = req.body;
      const newFighter = FighterService.createFighter(data);
      res.data = newFighter;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_BAD_REQUEST;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;
      const fighter = FighterService.getFighter(id);
      res.data = fighter;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateFighterValid,
  (req, res, next) => {
    try {
      const { id } = req.params;
      const data = req.body;
      const fighter = FighterService.updateFighter(id, data);
      res.data = fighter;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;
      const fighter = FighterService.deleteFighter(id);
      res.data = fighter;
    } catch (err) {
      res.err = err;
      err.httpStatus = HTTP_STATUS_ERROR_NOT_FOUND;      
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;