export const getDamage = (attacker, defender) => {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender); 
  return Math.max(0, damage);
}

export const getDamageCriticalHit = ({power}) => {
  return 2 * power;  
}

const getHitPower = ({power}) => {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  return power * criticalHitChance;  
}

const getBlockPower = ({defense}) => {
  // return block power
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance; 
}

