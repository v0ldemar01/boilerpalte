import { putExt, post, get } from "../requestHelper";
const entity = 'fights';

export const doAttack = async (id, body) => {
  return await putExt(entity, id, 'attack', body);
}

export const createFight = async (body) => {
  return await post(entity, body);
}

export const getFights = async () => {
  return await get(entity);
}
