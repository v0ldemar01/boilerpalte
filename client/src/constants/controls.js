export const controls = {
  PlayerOneAttack: 'a',
  PlayerOneBlock: 'd',
  PlayerTwoAttack: 'j',
  PlayerTwoBlock: 'l',
  PlayerOneCriticalHitCombination: ['q', 'w', 'e'],
  PlayerTwoCriticalHitCombination: ['u', 'i', 'o']
}
export const time = 10000;