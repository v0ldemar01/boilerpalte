import React, {useState, useEffect} from 'react';
import FighterImage from '../createFighter/fighterImage';
import Indicator from '../indicator';
import Winner from '../winner';
import { controls } from '../../constants/controls';
import {
  	getDamage,
  	getDamageCriticalHit  
} from '../../services/fightHelper';
import { doAttack } from '../../services/domainRequest/fightRequest';
import { time } from '../../constants/controls';
import './arena.css';

const {
	PlayerOneAttack, 
	PlayerOneBlock, 
	PlayerTwoAttack, 
	PlayerTwoBlock, 
	PlayerOneCriticalHitCombination, 
	PlayerTwoCriticalHitCombination
} = controls;

const Arena = props => {
	const [pressedKeys, setPressedKeys] = useState(new Set());
	const [fightersData, setFightersData] = useState({
		firstFighter: {},
		secondFighter: {},
	});

	const { firstFighter, secondFighter, fightId } = props;

	const initState = () => {    
		const state = Object.assign({}, fightersData);
		const data = props;   
		Object.entries(state)
			.filter((_, index) => index < 2)
			.forEach(([key]) => {     
				state[key].info = data[key];
				state[key].block = false;
				state[key].current_health = data[key].health;
				state[key].blockCriticalHit = false;
				state[key].indicatorWidth = 100;
			});    
		setFightersData(state);
	}

	const chooseAction = async () => {
		const fighters_block_status = [ 
			fightersData.firstFighter.block, 
			fightersData.secondFighter.block 
		]; 
		const fighters = [
			firstFighter,
			secondFighter
		];
		for (const key of pressedKeys) {      
			switch (key) {
				case PlayerOneAttack:             
					if (fighters_block_status.filter(e => e).length) return;            
					await fighterAction(...fighters, 'right');
					break;    
				case PlayerTwoAttack:  
					if (fighters_block_status.filter(e => e).length) return;
					await fighterAction(...[...fighters].reverse(), 'left');
					break;
				case PlayerOneBlock:
					setStatusFighterBlock('left', true);
					break;
				case PlayerTwoBlock:      
					setStatusFighterBlock('right', true);
					break;
				default:
					break;		
			}
		}
		
		const fighters_critical_hit_block_status = [
			fightersData.firstFighter.blockCriticalHit,
			fightersData.secondFighter.blockCriticalHit
		];

		const firstFighterCriticalHitAble = checkKeysCriticalHit(PlayerOneCriticalHitCombination);
		const secondFighterCriticalHitAble = checkKeysCriticalHit(PlayerTwoCriticalHitCombination);
		
		if (
			!fighters_critical_hit_block_status[0] &&
			firstFighterCriticalHitAble 
		) {      
			await fighterAction(...fighters, 'right', true);
		} else if (
			!fighters_critical_hit_block_status[1] &&
			secondFighterCriticalHitAble
		) {
			await fighterAction(...[...fighters].reverse(), 'left', true);
		}  
	}

	useEffect(
		() => {
			(
				async () => await initState()
			)()
		},
		[firstFighter, secondFighter]
	);  

	useEffect(
		() => {
			(
				async () => await chooseAction()
			)()
		},
		[pressedKeys]
	);    	

	const onKeyPressed = event => {
		const { key } = event;    
		toggleKeyboardKeys(key, 'add');        
	}	
	
	const onKeyUp = event => {
		const { key } = event;
		toggleKeyboardKeys(key, 'delete');
		switch (key) {
			case PlayerOneBlock:
				setStatusFighterBlock('left', false);  
				break; 
			case PlayerTwoBlock:      
				setStatusFighterBlock('right', false);  
				break;
			default:
				break;	
		}
	}

	const fighterAction = async (attacker, defender, position, isCriticalHit) => {
		let damage;    
		if (isCriticalHit) {
			const this_position = position === 'left' ? 'right' : 'left';
			setStatusFighterCriticalHitBlock(this_position, true);
			setTimeout(
				() => setStatusFighterCriticalHitBlock(this_position, false), time
			);
			damage = getDamageCriticalHit(attacker);     
		} else {
			damage = getDamage(attacker, defender);		
		} 
		setFighterHealth(position, damage);
		await doAttack(fightId, {
			fighter1Shot: position === 'right' ? damage : 0,
			fighter2Shot: position === 'left' ? damage : 0,
			fighter1Health: Math.max(0, fightersData.firstFighter.current_health),
			fighter2Health: Math.max(0, fightersData.secondFighter.current_health)
		});
		const newPercent = calcHealthIndicator(defender, position);
		setFighterIndicator(position, newPercent);
	}

	const setStatusFighterBlock = (position, status) => {
		const state = Object.assign({}, fightersData);
		if (position === 'left') {
			state.firstFighter.block = status;
		} else {
			state.secondFighter.block = status;
		}
		setFightersData(state);
	}  

	const setFighterHealth = (position, health) => {
		const state = Object.assign({}, fightersData);
		if (position === 'left') {
			state.firstFighter.current_health -= health;
		} else {
			state.secondFighter.current_health -= health;
		}
		setFightersData(state);
	}

	const setFighterIndicator = (position, percent) => {
		const state = Object.assign({}, fightersData);
		if (position === 'left') {
			state.firstFighter.indicatorWidth = percent;
		} else {
			state.secondFighter.indicatorWidth = percent;
		}
		setFightersData(state);
	}

	const checkKeysCriticalHit = keys => {     
		for (const key of keys) {
			if (!pressedKeys.has(key)) {
				return false;
			}
		}
		return true;
	}

	const setStatusFighterCriticalHitBlock = (position, status) => {
		const state = Object.assign({}, fightersData);
		if (position === 'left') {
			state.firstFighter.blockCriticalHit = status;
		} else {
			state.secondFighter.blockCriticalHit = status;
		}
		setFightersData(state);
	};
	
	
	const toggleKeyboardKeys = (key, action) => {
		const newSet = new Set(pressedKeys);
		if (action === 'add') {
			newSet.add(key);
		} else {
			if (pressedKeys.has(key)) {
				newSet.delete(key);
			}
		}
		setPressedKeys(newSet);
	};	

	const onExit = () => {
		const { setFightStatus } = props;
		setFightStatus(false);
	}

	const calcHealthIndicator = (fighter, position) => {
		const defenderInitHealth = fighter.health;
		const key = position === 'left' ? 'firstFighter' : 'secondFighter';
		const currentDefenderHealth = fightersData[key].current_health;  
		return currentDefenderHealth / defenderInitHealth * 100;  
	};  
	
	const checkEndGame = () => {
		if (fightersData.firstFighter.current_health <= 0) {
			return secondFighter;
		} else if (fightersData.secondFighter.current_health <= 0) {
			return firstFighter;
		} else {
			return false;
		}
	}
	const winner = checkEndGame();
	return (
		<>
			<div className="arena___root" onKeyDown={onKeyPressed} tabIndex={0} onKeyUp={onKeyUp}>
				<div className="arena___fight-status">
					<Indicator name={firstFighter.name} position="left" currentWidth={fightersData.firstFighter.indicatorWidth}/> 
					<Indicator name={secondFighter.name} position="right" currentWidth={fightersData.secondFighter.indicatorWidth}/> 
				</div>
				<div className="arena___battlefield">
					<FighterImage fighter={firstFighter} position="left" arena/>
					<FighterImage fighter={secondFighter} position="right" arena/>
				</div>
			</div>
			{ winner && <Winner fighter={winner} onClose={onExit}/> }
		</>		
	);
}

export default Arena;