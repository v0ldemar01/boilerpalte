import React, { useState } from 'react';
import { FormControl, InputLabel, makeStyles, Select } from '@material-ui/core';
import { MenuItem } from 'material-ui';
import FighterImage from '../createFighter/fighterImage';
import FighterInfo from '../createFighter/fighterInfo';

const useStyles = makeStyles((theme) => ({
    formControl: {        
        display: 'flex',
        margin: '20px auto',
        width: '50%',        
    },
    selectEmpty: { 
        marginTop: theme.spacing(2),
    },
}));

export default function Fighter({ fightersList, onFighterSelect, selectedFighter, position }) {
    const classes = useStyles();
    const [fighter, setFighter] = useState();

    const handleChange = (event) => {
        debugger;
        setFighter(event.target.value);
        onFighterSelect(event.target.value);
    };

    return (
        <div>
            <FormControl className={classes.formControl}>
                {fightersList && 
                    <InputLabel id="simple-select-label">Select Fighter</InputLabel>}
                {onFighterSelect && 
                    <Select
                        labelId="simple-select-label"
                        id="simple-select"
                        value={fighter}
                        onChange={handleChange}
                    >
                {fightersList.map((it, index) => {
                    return (
                        <MenuItem key={`${index}`} value={it}>{it.name}</MenuItem>
                    );
                })}
            </Select>}
                
                {selectedFighter
                    ? <div>
                        <FighterImage fighter={selectedFighter} position={position}/>
                        <FighterInfo fighter={selectedFighter} />                        
                    </div>
                    : null
                }
            </FormControl>
        </div>
    )
}
