import React from 'react';
import './fighterImage.css'

export default function FighterImage(props) {  
	const { fighter: { source, name }, position, arena, history } = props;
	const classBlock = arena ? 
		`arena___fighter ${position ? `arena___${position}-fighter` : ''}` :
		history ? 'fighters___fighter' : 
		`fighter-preview___${position}`;
	return (
		<div className={classBlock}>
			<img className={history ? "fighter___fighter-image" : "fighter-preview___img"} src={source} alt={name} />
		</div>
	);
};