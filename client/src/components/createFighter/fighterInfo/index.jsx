import React from 'react';
import Health from './svg/heartbeat.svg';
import Attack from './svg/boxing.svg';
import Defense from './svg/defense.svg';

import './fighterInfo.css';

export default function FighterInfo(props) {
	const { fighter } = props;  
	const { name } = fighter;
	
	const svg_obj = {
		'health': Health,
		'power': Attack,
		'defense': Defense,
	};

	const renderItems = () => {
		return Object.entries(fighter)
		.map(([key, value]) => {
			const svg_value = svg_obj[key];
			if (!svg_value) {
				return null;
			}
			return (
				<div class="fighter-preview___info_row">
					<span>
						<img src={svg_value} alt={key}/>
					</span>
					<span>
						{ value }
					</span>
				</div>
			);      
		})
		.filter(e => e);  
	}
	const items = renderItems();

	return (
		<div className="fighter-preview___info">
			<h2 class="fighter-preview___info_name">{name}</h2>
			{ items }
		</div>
	);
};