import React from 'react';
import { getFighters } from '../../services/domainRequest/fightersRequest';
import { createFight } from '../../services/domainRequest/fightRequest'
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import History from '../history';
import { Button } from '@material-ui/core';

import './fight.css'

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        activeHistory: false,
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if(fighters && !fighters.error) {
            this.setState({ fighters });
        }
    }

    onFightStart = async () => {
        const { setFightStatus, setNewFightersDetails } = this.props;
        const { fighter1, fighter2 } = this.state;
        const { id } = await createFight({
            fighter1,
            fighter2,
            log: []
        });
        setNewFightersDetails(fighter1, fighter2, id);
        setFightStatus(true);
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    toggleHistory = () => {
        this.setState((state) => {
            return {
                ...state,
                activeHistory: !state.activeHistory
            }
        });
    }

    render() {
        const  { fighter1, fighter2 } = this.state;
        return (
            <div id="wrapper">
                <NewFighter onCreated={this.onCreate} />
                <div id="figh-wrapper">
                    <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} position="left"/>
                    <div className="btn-wrapper">
                        <Button onClick={this.onFightStart} variant="contained" color="primary" disabled={!(fighter1 && fighter2)}>Start Fight</Button>
                    </div>
                    <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} position="right"/>
                </div>
                <div className="btn-wrapper">
                    <Button onClick={this.toggleHistory} variant="contained" color={this.state.activeHistory ? "secondary" : "primary" }>History</Button>
                </div>  
                {this.state.activeHistory && <History />}              
            </div>
        );
    }
}

export default Fight;