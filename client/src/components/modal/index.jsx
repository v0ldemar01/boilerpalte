import React from 'react';
import FighterImage from '../createFighter/fighterImage';
import './modal.css';

const Modal = props => {
	const { fighter, onClose } = props;
	const { name } = fighter;
  	return (
		<div className="arena_root">
			<div className="modal-layer">
				<div className="modal-root">
					<div className="modal-header">
						Winner {name}
						<div className="close-btn" onClick={onClose}>
							×
						</div>
					</div>
					<FighterImage fighter={fighter} arena/>
				</div>				
			</div>
		</div>
  )
} 

export default Modal;