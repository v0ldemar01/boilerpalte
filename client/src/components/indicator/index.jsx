import React, { useState, useEffect } from 'react';
import './indicator.css'

export default function Indicator(props) {  
  const [width, setWidth] = useState(100);
  const { name, position, currentWidth } = props;
  const blockId = `${position}-fighter-indicator`;

  useEffect(
    () => {
      setWidth(currentWidth)
    }, 
    [currentWidth]
  );
  
  return (
    <div className="arena___fighter-indicator">
      <span className="arena___fighter-name">
        { name}
      </span>
      <div class="arena___health-indicator">
        <div class="arena___health-bar" id={blockId} style={{ width: `${width < 0 ? 0 : width}%`}}></div>
      </div>
    </div>
  );
}