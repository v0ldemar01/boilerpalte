import * as React from 'react';
import SignInUpPage from '../signInUpPage';
import { isSignedIn } from '../../services/authService';
import Fight from '../fight';
import Arena from '../arena';
import SignOut from '../signOut';

class StartScreen extends React.Component {
    state = {
        isSignedIn: false,
        isFightStatus: false,
        fighter1: {},
        fighter2: {},
        fightId: '',
    };

    componentDidMount() {
        this.setIsLoggedIn(isSignedIn());
    }

    setIsLoggedIn = (isSignedIn) => {
        this.setState((state) => {
            return {
                ...state, isSignedIn
            }
        })
    }

    setFightStatus = (newStatus) => {
        this.setState((state) => {
            return {
                ...state, isFightStatus: newStatus
            }
        });        
    }

    setNewFightersDetails = (fighter1, fighter2, fightId) => {
        this.setState((state) => {
            return {
                ...state, fighter1, fighter2, fightId
            }
        });
    }

    render() {
        const { isSignedIn, isFightStatus, fighter1, fighter2, fightId } = this.state;
        if (!isSignedIn) {
            return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn} />
        }
        
        if (isFightStatus) {
            return <Arena firstFighter={fighter1} secondFighter={fighter2} fightId={fightId} setFightStatus={this.setFightStatus} />
        }

        return (
            <>
                <Fight setFightStatus={this.setFightStatus} setNewFightersDetails={this.setNewFightersDetails}/>
                <SignOut isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)} />
            </>
        );
    }
}

export default StartScreen;