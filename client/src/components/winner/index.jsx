import React from 'react';
import Modal from '../modal';

const Winner = props => {
  const { fighter, onClose } = props;
  return (
    <Modal fighter={fighter} onClose={onClose}/>
  );
}

export default Winner;