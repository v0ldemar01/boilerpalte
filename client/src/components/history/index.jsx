import React, { useState, useEffect } from 'react';
import { getFights } from '../../services/domainRequest/fightRequest';
import Fighter from '../fighter';
import CircularProgress from '@material-ui/core/CircularProgress';
import Defense from '../createFighter/fighterInfo/svg/defense.svg';

const History = props => {
	const [ history, setHistory ] = useState(null);
	useEffect(
		() => {
			getFights()
				.then(historyItem => {
					setHistory(historyItem)
				})		
		},
		[]		
	);
	
	const renderItems = () => {
		return Array.isArray(history) ? history.map(item => {
			const {fighter1, fighter2, winner, log} = item;
			return (
				<div id="figh-wrapper" style={{textAlign: 'center'}}>
					<Fighter 
						selectedFighter={fighter1} 									 								
						position="left" 									
					/>
					<div >
						<h3 >Winner:<br />{item[winner].name}</h3>
						<dl>
							{ log.map(({fighter1Shot, fighter1Health, fighter2Health}) => {
								return (
									<dt style={{border: '1px solid red', padding: '10px', margin: '10px'}}>
										<div><h4>Attaker:<br />{fighter1Shot ? fighter1.name : fighter2.name}</h4></div>
										<div><h4>Defender:<br />{fighter1Shot ? fighter2.name : fighter1.name}</h4></div>
										<div>
											<h3>Result:</h3>
											<span><img src={Defense} alt={fighter1.name}/></span>
											<span style={{fontSize: "18pt"}}>{fighter1Health.toFixed(2)}</span>&ensp;
											<img src={Defense} alt={fighter2.name}/> 
											<span style={{fontSize: "18pt"}}>{fighter2Health.toFixed(2)}</span>
										</div>
									</dt>
								)
							})}
						</dl>						
					</div>					
					<Fighter 
						selectedFighter={fighter2} 									 								
						position="right"									
					/>					
				</div>			
			)
		}) : null;			
	} 
	const items = renderItems();
	return (
		<>
			{ !history && <CircularProgress style={{ left: '15%', position: "relative"}}/>}
			{items}			
		</>		
	)
}

export default History;