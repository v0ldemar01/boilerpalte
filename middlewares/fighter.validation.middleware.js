const { fighter } = require('../models/fighter');
const { createValid } = require('../utils/createValid');
const {     
    HTTP_STATUS_ERROR_BAD_REQUEST
} = require('../constants');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {
        createValid(req, fighter, validateFields);
        next();
    } catch (err) {
        return res.status(HTTP_STATUS_ERROR_BAD_REQUEST)
            .json({
                error: true,
                message: err.message,
            });
    }     
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        createValid(req, fighter, validateFields);
        next();
    } catch (err) {
        return res.status(HTTP_STATUS_ERROR_BAD_REQUEST)
            .json({
                error: true,
                message: err.message,
            });
    }
    next();
}

const validateFields = ({
    name,
    health,
    power,
    defense
}) => {    
    validateName(name);
    validateHealth(health);
    validatePower(power);    
    validateDefense(defense);
}
const validateName = trial_name => {
    if (!trial_name.trim()) {
        throw new Error(`Name field must be filled!`);
    } else if (!isNaN(parseInt(trial_name.trim()))) {
        throw new Error(`Fighter name must be string`);
    } else if (trial_name.trim().length < 3) {
        throw new Error(`Name field length is too short!`);
    }
} 

const validateHealth = trial_health => {
    if (trial_health === null) {
        throw new Error(`Health field must be filled!`);
    } else if (typeof trial_health !== 'number') {
        throw new Error(`Fighter health must be number`);
    } else if (trial_health <= 0) {
        throw new Error(`Fighter health must be above 0`);
    }    
}

const validatePower = trial_power => {
    if (trial_power === null) {
        throw new Error(`Fighter power field must be filled!`);
    } else if (typeof trial_power !== 'number') {
        throw new Error(`Fighter power must be number`);
    } else if (
        trial_power <= 0 || 
        trial_power >= 100
    ) {
        throw new Error(`Fighter power must be above 0, but less than 100`);
    }  
}

const validateDefense = trial_defense => {
    if (trial_defense === null) {
        throw new Error(`Fighter defense field must be filled!`);
    } else if (typeof trial_defense !== 'number') {
        throw new Error(`Fighter defense must be number`);
    } else if (
        trial_defense <= 0 ||
        trial_defense > 10
    ) {
        throw new Error(`Fighter defense must be in the range from 1 to 10`);
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;