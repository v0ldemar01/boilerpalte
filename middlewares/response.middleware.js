const { HTTP_STATUS_SUCCESS_OK } = require('../constants');

const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.err) {
        const { message, httpStatus } = res.err;
        return res.status(httpStatus).json({
            error: true,
            message
        });
    } else if (res.data) {
        return res.status(HTTP_STATUS_SUCCESS_OK).json(res.data);
    }    
    next();
}

exports.responseMiddleware = responseMiddleware;