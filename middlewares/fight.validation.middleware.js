const { fight } = require('../models/fight');
const { attack } = require('../models/attack');
const { createValid } = require('../utils/createValid');
const {     
    HTTP_STATUS_ERROR_BAD_REQUEST
} = require('../constants');

const createFightValid = (req, res, next) => {
    // TODO: Implement validatior for fight entity during creation
    try {
        createValid(req, fight, validateFightFields);
        next();
    } catch (err) {
        return res.status(HTTP_STATUS_ERROR_BAD_REQUEST)
            .json({
                error: true,
                message: err.message,
            });
    }     
}

const updateFightValid = (req, res, next) => {
    // TODO: Implement validatior for fight entity during update
    try {
        createValid(req, attack, validateAttackFields);
        next();
    } catch (err) {
        return res.status(HTTP_STATUS_ERROR_BAD_REQUEST)
            .json({
                error: true,
                message: err.message,
            });
    }
    next();
}

const validateFightFields = ({
	fighter1,
	fighter2
}) => {    
    validateFighters(fighter1, fighter2);    
}

const validateAttackFields = (params) => {       	  
	Object.values(params).forEach(other_element => validateValue(other_element));
}

const validateFighters = (trial_fighter1, trial_fighter2) => {
    if (!trial_fighter1 || !trial_fighter2) {
        throw new Error(`Choose 2 fighters!`);
    } 
} 

const validateValue = trial_value => {
    if (typeof trial_value !== 'number') {
        throw new Error(`Error, value must be a number!`);
    }  
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;