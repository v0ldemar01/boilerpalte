const { user } = require('../models/user');
const { createValid } = require('../utils/createValid');
const {     
    HTTP_STATUS_ERROR_BAD_REQUEST
} = require('../constants');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        createValid(req, user, validateFields);
        next();
    } catch (err) {
        return res.status(HTTP_STATUS_ERROR_BAD_REQUEST)
            .json({
                error: true,
                message: err.message,
            });
    }    
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        createValid(req, user, validateFields);
        next();
    } catch (err) {
        return res.status(HTTP_STATUS_ERROR_BAD_REQUEST)
            .json({
                error: true,
                message: err.message,
            });
    }    
}

const validateFields = ({
    email,
    phoneNumber,    
    firstName,
    lastName,
    password
}) => {
    validateEmail(email);
    validatePhone(phoneNumber);
    validateName(firstName, lastName);
    validatePassword(password);
}

const validateEmail = trial_email => {
    if (!trial_email.trim()) {
        throw new Error(`Email field must be filled!`);
    }
    const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@gmail.com$/;
    const check_regexp = trial_email.match(regex);
    if (!check_regexp) {
        throw new Error(`Email must be like name@gmail.com!`);
    } 
}

const validatePhone = trial_phone => {
    if (!trial_phone.trim()) {
        throw new Error(`Phone number field must be filled!`);
    }
    const regex = /^\+380(\d{9})$/;
    const check_regexp = trial_phone.match(regex);
    if (!check_regexp) {
        throw new Error(`Phone number must be like +380xxxxxxxxx!`);
    }
}

const validatePassword = trial_password => {
    if (!trial_password.trim()) {
        throw new Error(`Password field must be filled!`);
    } else if (trial_password.trim().length < 3) {
        throw new Error(`Password length is too short!`);
    }
}

const validateName = (trial_first_name, trial_last_name) => {
    if (!trial_first_name.trim() || !trial_last_name.trim()) {
        throw new Error(`First name and last name field must be filled!`);
    } else if (trial_first_name.trim().length < 3) {
        throw new Error(`First name length is too short!`);
    } else if (trial_last_name.trim().length < 3) {
        throw new Error(`Last name length is too short!`);
    }
} 

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;